#include <iostream>
using namespace std;

int main() {
    char cO;
    int a;
    int b;

    cout << "Enter operation(a/s/m/p) " <<endl;
    cin >> cO;

    cout << "Enter operand 1:" <<endl;
    cin >> a;
    cout << "Enter operand 2:" <<endl;
    cin >> b;

    if(cO == 'a')
        cout << a << " + " << b << " is " << a+b<<endl;
    else if(cO == 's')
        cout << a << " - " << b << " is " << a-b<<endl;
    else if(cO == 'm')
        cout << a << " * " << b << " is " << a*b<<endl;
    else if(cO == 'p')
        cout << a << " / " << b << " is " << a/b<<endl;
    return 0;
}
