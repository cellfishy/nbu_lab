#include <iostream>
#include <string>
using namespace std;

int main() {
    string sName;

    cout << "Input first and last name:" <<endl;
    getline(cin, sName);

    cout << sName.substr(sName.find(" ")+1) << ", " << sName.substr(0, sName.find(" ")) <<endl;
    return 0;
}
