/***
FN:F73600
PID:1
GID:1
*/

#include <iostream>
#include <vector>

using namespace std;

double doMagic(vector<double> vFirst, vector<double> vSecond) {
    double dResult = 0;
    for (int i=0; i<vFirst.size();i++){

        dResult = dResult + (vFirst[i] * vSecond[i]);
    }
    return dResult;
}

int main() {
    int iC, i = 0;
    double dNumber;
    cin >> iC;
    vector<double> vFirst, vSecond;

    for (int i=0; i<iC; i++) {
        cin >> dNumber;
        vFirst.push_back(dNumber);
    }

    for (int i=0; i<iC; i++) {
        cin >> dNumber;
        vSecond.push_back(dNumber);
    }

    cout << doMagic(vFirst, vSecond) << endl;

	return 0;
}
