/***
FN:F73600
PID:3
GID:1
*/

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <stdlib.h>
#include <algorithm>

using namespace std;

void insert(vector<int>& v, int pos, int s) {
   int last = v.size();
   v.push_back(v[last]);
   for (int i = last; i > pos; i--)
      v[i] = v[i - 1];

   v[pos] = s;
}

void shift_right(vector<int>& v) {
    vector<int> vTemp;
    int iLast = v.back(), iTmp;
    vTemp.push_back(iLast);

    for(int r = 0; r < v.size()-1; r++) {
        vTemp.push_back(v[r]);
    }
    v = vTemp;
}

void shift_left(vector<int>& v) {
    vector<int> vTemp;
    int iFirst = v.front(), iTmp;
    v.erase(v.begin());
    for(int r = 0; r < v.size(); r++) {
        vTemp.push_back(v[r]);
    }

    vTemp.push_back(iFirst);
    v = vTemp;
}
int main() {
    int iC, i = 0;
    int dNumber;
    cin >> iC;
    vector<int> vFirst;


    for (int i=0; i<iC; i++) {
        cin >> dNumber;
        vFirst.push_back(dNumber);
    }

    while (i < 1) {
        int a = 0;
        string response, aExecute[3];

        getline(cin, response);
        stringstream ssin(response);
        while (ssin.good() && a < 3) {
            ssin >> aExecute[a];
            a++;
        }
        if(aExecute[0] == "add") {
            insert(vFirst, atoi(aExecute[1].c_str())-1, atoi(aExecute[2].c_str()));
        } else if(aExecute[0] == "remove") {
            vFirst.erase(vFirst.begin() + atoi(aExecute[1].c_str())-1);
        } else if(aExecute[0] == "print") {
            for(int s = 0; s < vFirst.size(); s++) {
                cout << vFirst[s] << ' ';
            }
            cout << endl;
        } else if(aExecute[0] == "shift_right") {
            shift_right(vFirst);
        } else if(aExecute[0] == "shift_left") {
            shift_left(vFirst);
        } else if(aExecute[0] == "exit")
            return 0;
    }

	return 0;
}

