/***
FN:F73600
PID:2
GID:1
*/

#include <string>
#include <iostream>
#include <sstream>
#include <iterator>
#include <vector>

using namespace std;
vector<int> vFirst;

void doMagic(vector<int> &vFirst) {
    int mid  = vFirst.size() / 2;
    int last = vFirst.size() - 1;

    for (int i = 0; i < mid; i++) {
      int tmp = vFirst[i];
      vFirst[i] = vFirst[last - i];
      vFirst[last - i] = tmp;
    }
}


int main() {

    string line;

    getline(cin, line);
    istringstream os(line);

    int i;
    while(os >> i)
        vFirst.push_back(i);

    doMagic(vFirst);

    for(int s = 0; s < vFirst.size(); s++)
        cout << vFirst[s] << ' ';

	return 0;
}
