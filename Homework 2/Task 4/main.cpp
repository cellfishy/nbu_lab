#include <iostream>
#include <string>
#include <math.h>

using namespace std;

class Triangle {
    private:
        double dA, dB, dC;
    public:
        Triangle(double dSideA, double dSideB, double dSideC): dA(dSideA), dB(dSideB), dC(dSideC){}

        double get_perimeter() {
            return dA + dB + dC;
        }

        double get_area() {
            double p = (dA + dB + dC) / 2;
            return sqrt(p*(p-dA)*(p-dB)*(p-dC));
        }

        bool exists(double dA, double dB, double dC) const
        {
            return dA + dB > dC && dA + dC > dB && dB + dC > dA;
        }
};

int main()
{
    double a, b, c;
    cin >> a >> b >> c;

    Triangle t(a, b, c);

    if(!t.exists(a, b, c))
    {
        cout << "No such triangle!" << endl;
        return 0;
    }

    cout << t.get_perimeter() << endl;
    cout << t.get_area() << endl;

    return 0;
}

