#include <iostream>
#include <string>
#define dPi 3.14

using namespace std;

class Circle
{
private:
    double r;
public:
    Circle(double radius): r(radius)
    {
    }

    double get_radius()
    {
        return r;
    }

    double get_perimeter()
    {
        return 2 * dPi * r;
    }

    double get_area() {
        return dPi * r * r;
    }


};

int main()
{
    double radius;
    cin >> radius;

    Circle c(radius);

    cout << c.get_radius() << endl;
    cout << c.get_perimeter();
    cout << c.get_area();//for homework

    return 0;
}
