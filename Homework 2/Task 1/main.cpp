#include <iostream>
#include <stdio.h>
using namespace std;

int main() {
    string sString;

    cout << "Input string:" << endl;
    cin >> sString;

    if(sString == string(sString.rbegin(), sString.rend()))
        cout << "Both ways match" << endl;
    else
        cout << "String doesn't match both ways" << endl;

    return 0;
}
