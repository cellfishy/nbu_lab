#include <iostream>

using namespace std;

string asCode[255];
double adScore[255];

void doMath(int iCount){
    for(int i = 0; i < iCount; ++i){
        asCode[i] += "\t";

        if(adScore[i] >= 3.0 && adScore[i] <= 6.0)
            asCode[i] += "[PASS]";
        else if(adScore[i] < 3.0 && adScore[i] >= 2.0)
            asCode[i] += "[FAIL]";
        else
            asCode[i] += "[INVALID]";
    }
}

int main() {
    int i = 0;

    while(cin >> asCode[i] >> adScore[i])
        i++;

    doMath(i);

    for(int a = 0; a < i; a++)
        cout << asCode[a] << endl;

    return 0;
}
