#include <iostream>

using namespace std;


int iMinimum(int a) {
    int iLowest = a;

    while(a > 0){
        int iLast = a % 10;
        a = a / 10;
        if((iLast) < iLowest){
            iLowest = iLast;
        }
    }

    return iLowest;
}

int main() {
    int iInput;
    int iResult;

    cin >> iInput;

    iResult = iMinimum(iInput);

    cout << iResult << endl;

    return 0;
}
