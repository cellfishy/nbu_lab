#include <iostream>

using namespace std;

int main() {

    int iCount, i = 0;
    double dSum = 0, dInput;

    cin >> iCount;

    while(i < iCount){
        cin >> dInput;
        dSum = dInput + dSum;
        ++i;
    }

    cout << dSum << endl;

    return 0;
}

