#include <iostream>
using namespace std;

/*
2...9            -                     Two,Three,Found...Nine
A                 -                     Ace
J                 -                      Jack
Q                 -                     Queen
K                 -                     King
D                 -                     Diamonds
H                 -                     Hearts
S                 -                     Spades
C                 -                     Clubs
Input: QS

Output: Queen of spades

*/

int main() {
    string a;
    cin >> a;
    char b = a[0];

    if(a.size() == 1) {
        if(b == '2')
            cout << "Two" <<endl;
        else if(b == '3')
            cout << "Three" <<endl;
        else if(b == '4')
            cout << "Four" <<endl;
        else if(b == '5')
            cout << "Five" <<endl;
        else if(b == '6')
            cout << "Six" <<endl;
        else if(b == '7')
            cout << "Seven" <<endl;
        else if(b == '8')
            cout << "Eight" <<endl;
        else if(b == '9')
            cout << "Nine" <<endl;
    } else if(a.size() == 2) {
        char c = a[1];

        if(b == 'A')
            cout << "Ace";
        else if(b == 'J')
            cout << "Jack";
        else if(b == 'Q')
            cout << "Queen";
        else if(b == 'K')
            cout << "King";
        else if(b == 'D')
            cout << "Diamonds";
        else if(b == 'H')
            cout << "Hearts";
        else if(b == 'S')
            cout << "Spades";
        else if(b == 'C')
            cout << "Clubs";

        cout << " of ";

        if(c == 'A')
            cout << "Ace";
        else if(c == 'C')
            cout << "Jack";
        else if(c == 'Q')
            cout << "Queen";
        else if(c == 'K')
            cout << "King";
        else if(c == 'D')
            cout << "Diamonds";
        else if(c == 'H')
            cout << "Hearts";
        else if(c == 'S')
            cout << "Spades";
        else if(c == 'C')
            cout << "Clubs";
    }
    return 0;
}
