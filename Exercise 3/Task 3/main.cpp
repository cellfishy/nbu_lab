#include <iostream>
using namespace std;

/*
Дадена е следната таблица

A - 4
B - 3
C - 2
D - 1
F - 0
Възможно е да има + или - накрая, които добавявят и махат респективно по 0.3
(има 2 изключения - A+ = 4, F- = 0, вместо на 4.3 и -0.7)

Input: A+

Output: 4

Input: C+

Output: 2.3

Input: F-

Output: 0
*/

int main() {
    string a;
    float c;

    cin >> a;
    char b = a[0];

    if(b == 'A')
        c = 4;
    else if (b == 'B')
        c = 3;
    else if (b == 'C')
        c = 2;
    else if (b == 'D')
        c = 1;
    else if (b == 'F')
        c = 0;

    if(a.size() > 1) {
        if(a[1] == '+' && b != 'A')
            c = c + 0.3;
        else if(a[1] == '-' && b != 'F')
            c = c - 0.3;
    }

    cout << c <<endl;

    return 0;
}
