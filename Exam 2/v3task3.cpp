#include <iostream>

using namespace std;
string sResult;

void doMagic(string sInput) {
    int a = 0;

    while(a < sInput.size()) {
        if(isdigit(sInput[a]))
            sResult += sInput[a];
        a++;
    }
    sResult += '\n';
}

int main() {

    int iCount, i = 0;

    cin >> iCount;

    string asInput[iCount];

    while(i < iCount){
        cin >> asInput[i];
        doMagic(asInput[i]);
        ++i;
    }

    cout << sResult << endl;

    return 0;
}

