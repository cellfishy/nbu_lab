#include <iostream>

using namespace std;

int asNumbers[255];
int iResult = 0;

int main() {
    int i = 0;

    while(cin >> asNumbers[i]) {
        int length = 1;
        int iDigits = asNumbers[i];

        while ( iDigits /= 10 )
           length++;

        if(length == 1 || length == 3)
            iResult = iResult + asNumbers[i];

        i++;
    }

    cout << iResult << endl;

    return 0;
}
