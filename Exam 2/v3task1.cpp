#include <iostream>

using namespace std;

int main() {

    int iCount, i = 0;
    double dNum = 0, dMax = 0;

    cin >> iCount;

    while(i < iCount){
        cin >> dNum;
        if(dNum > dMax)
            dMax = dNum;
        ++i;
    }

    cout << dMax << endl;

    return 0;
}

