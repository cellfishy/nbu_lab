#include <iostream>
using namespace std;

int main() {
    string sName;
    float fWage;
    int iHours;

    cout << "Enter employee:" <<endl;
    cin >> sName;
    cout << "Enter his hourly wage: " <<endl;
    cin >> fWage;
    if(fWage < 0) {
        cout << "You can't have negative salary!" <<endl;
        return 1;
    }
    cout << "Enter how many hours the employee has worked last week:" <<endl;
    cin >> iHours;
    if(iHours < 0) {
        cout << "You should work more!" <<endl;
        return 1;
    }

    if(iHours > 168) {
        cout << "You've overworked! Payment goes for medical bills!" <<endl;
        return 1;
    }

    if(iHours <= 42) {
        cout << "The salary of " << sName << " or the last week is " << fWage * iHours << " BGN." <<endl;
    } else {
        float fSalary = 0;
        fSalary = (fWage * 42) + ((1,1 * fWage) * iHours - 42);
        cout << "The salary of " << sName << " or the last week is " << fWage * iHours << " BGN." <<endl;
    }

    return 0;
}
