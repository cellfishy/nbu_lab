#include <iostream>
#include <stdio.h>
#include <iomanip>
using namespace std;

/*
Пр.1
Output: Enter your ownership in KTB
Input: 1 2
Output: You have 50% shares or 34000000 BGN.

Пр.2
Output: Enter your ownership in KTB
Input: 11 10
Output: You can't own more than 100%

Пр.3
Output: Enter your ownership in KTB
Input: -1 10
Output: You can't own less than 0%
*/

int main() {
    double iA, iB;
    double iC;

    cout << "Enter your ownership in KTB" <<endl;
    cin >> iA >> iB;
    if((iA/iB) * 100 > 100) {
        cout << "You can't have more than 100%";
        return 1;
    }

    if((iA/iB) * 100 < 0) {
        cout << "You can't own less than 0";
        return 1;
    }

    cout << "You have " << (iA/iB) * 100 << "% shares or ";
    cout << fixed << (iA/iB) * 68000000 << setprecision(2);
    return 0;
}
