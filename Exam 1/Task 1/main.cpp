#include <iostream>
using namespace std;

int main() {
    int iA;
    int iB;
    int iC;

    string sResult = "";

    cout << "Enter a number: " <<endl;
    cin >> iA;

    for(iB = 2; iB <= 5; iB++) {
        cout << iA << "^" << iB << " = ";
        iC = iA;
        for(int iD = 1; iD < iB; iD++)
            iC = iC * iA;
        cout << iC;
        cout << ", ";
    }

    return 0;
}
